﻿using System;
using System.Web.Mvc;
using Exercise3.Services;
using System.Diagnostics;

namespace Exercise3.Controllers
{
    public class EvidenceController : Controller
    {

        //
        // GET: /EvidenceController/
        //Displays the Evidence Summary in the form of a PDF file.
        public void Index()
        {
            String packageId = Request.QueryString["packageId"];
            string API_KEY = (string)HttpContext.Session["API_KEY"];
            string BASE_URL = (string)HttpContext.Session["BASE_URL"];

            if (string.IsNullOrEmpty(packageId))
            {
                throw new SystemException("packageId parameter is empty or null, please provide a valid package id");
            }

            EvidenceService svc = new EvidenceService();
            byte[] evidenceSummary = svc.downloadEvidenceSummary(packageId, API_KEY, BASE_URL);
            Trace.WriteLine("Evidence content retrieved..");
            Trace.WriteLine("Length of byte array: " + evidenceSummary.Length);

            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Length", evidenceSummary.Length.ToString());
            Response.BinaryWrite(evidenceSummary);
            Response.Flush();
            Response.End();
        }
	}
}