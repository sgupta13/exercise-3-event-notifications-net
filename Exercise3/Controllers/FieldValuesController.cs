﻿using System;
using System.Web.Mvc;
using Exercise3.Services;

namespace Exercise3.Controllers
{
    public class FieldValuesController : Controller
    {
        //
        // GET: /FieldValues/
        // Displays the values found in the fields of signed and completed documents 
        public void Index()
        {
            String packageId = Request.QueryString["packageId"];
            string API_KEY = (string)HttpContext.Session["API_KEY"];
            string BASE_URL = (string)HttpContext.Session["BASE_URL"];

            if (string.IsNullOrEmpty(packageId))
            {
                throw new SystemException("packageId parameter is empty or null, please provide a valid package id");
            }

            FieldValuesService svc = new FieldValuesService();
            string fieldValues = svc.getFieldValues(packageId, API_KEY, BASE_URL);

            Response.ContentType = "text/plain";
            Response.Write(fieldValues);
            Response.Flush();
            Response.End();
        }
	}
}