﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Diagnostics;

namespace Exercise3.Database
{
    //Stores the e-SignLive notifications in a database.
    //Stores the package id along with its status in a database.
    public class NotificationsDatabase 
    {
        private static NotificationsDatabase instance;
        private Dictionary<string, string> myDictionary = new Dictionary<string, string>();

        private NotificationsDatabase() { }

        /// <summary>
        /// Create a fake database to store the package IDs and their corresponding status
        /// </summary>
        public static NotificationsDatabase getInstance
        {
          get 
          {
             if (instance == null)
             {
                 instance = new NotificationsDatabase();
             }
             return instance;
          }
       }

        //Saves the package Id and the status to the database
        public void saveToDatabase(string packageId, string status)
        {
            myDictionary[packageId] = status;
        }

        //Returns the database entries 
        public Dictionary<String, String> getFromDatabase()
        {
            return myDictionary;
        }

	}
}