﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Exercise3.Models
{
    //Notification model and enum containing all the "supported" eslNotification types
    public class ESLNotificationsModel
    {
        public string name { get; set; }
        public string sessionUser { get; set; }
        public string packageId { get; set; }
        public string message { get; set; }
    }
}