﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Exercise3.Models
{
    // Notification model and enum containing all the "supported" eslNotification types
    public enum NotificationEventsType
    {
            PACKAGE_ACTIVATE, 
            PACKAGE_COMPLETE, 
            PACKAGE_CREATE,
            PACKAGE_DEACTIVATE,
            PACKAGE_DECLINE,
            PACKAGE_DELETE,
            PACKAGE_READY_FOR_COMPLETION,
            PACKAGE_RESTORE,
            PACKAGE_TRASH,
            PACKAGE_EXPIRE, 
            PACKAGE_OPT_OUT,  
            SIGNER_COMPLETE, 
            DOCUMENT_SIGNED,
            ROLE_REASSIGN
    }
}