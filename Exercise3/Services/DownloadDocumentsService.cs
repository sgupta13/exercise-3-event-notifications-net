﻿using System;
using Silanis.ESL.SDK;
using System.Diagnostics;

namespace Exercise3.Services
{
    //Downloads the signed documents and the documents zip file
    public class DownloadDocumentsService
    {
        public byte[] downloadZippedDocuments(String packageId, string API_KEY, string BASE_URL)
        {
            PackageId pkgId = new PackageId(packageId);

            EslClient client = new EslClient(API_KEY, BASE_URL);
            Trace.WriteLine("eSL client for download documents created..");

            byte[] documentsZipBinary = client.DownloadZippedDocuments(pkgId);
            Trace.WriteLine("Documents .zip binary retrieved..");
            Trace.WriteLine("Length of byte array: " + documentsZipBinary.Length);

            return documentsZipBinary;
        }
    }

}