﻿using System;
using Silanis.ESL.SDK;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using Exercise3.Models;
using Silanis.ESL.SDK.Builder;
using System.IO;
using System.Globalization;
using System.Web;
using Exercise3.Properties;

namespace Exercise3.Services
{
    //Package creation with e-SignLive
    public class ESLPackageCreation
    {
        readonly static string CUSTOM_ID = "SIGNER_1";

        public static PackageId createPackage(InsuranceFormModel model, string API_KEY, string BASE_URL, string AUTO_SUBMIT_URL)
        {
            String insuredInitials = model.insuredInitials;
            String emailAddress = model.emailAddress;
            String firstName = model.firstName;
            String lastName = model.lastName;
            String language = model.language;
            String address = model.address;
            String city = model.city;
            String province = model.province;
            String vehicleModel = model.vehicleModel;
            String vehicleMake = model.vehicleMake;
            String vehicleModelYear = model.vehicleModelYear;
            String vehicleColor = model.vehicleColor;

            EslClient client = new EslClient(API_KEY, BASE_URL);

            Stream file = new MemoryStream(Properties.Resources.Insurance_Company_Contract_Final_Version);

            // Build the DocumentPackage object
            DocumentPackage documentPackage = PackageBuilder.NewPackageNamed("C# Insurance Policy Package " + DateTime.Now)

                // Customizing setting
                .WithSettings(DocumentPackageSettingsBuilder.NewDocumentPackageSettings()
                                .WithDecline()
                                .WithOptOut()
                                .WithDocumentToolbarDownloadButton()
                                .WithHandOverLinkHref(AUTO_SUBMIT_URL)
                //TODO: Customize the HandOverLinkText and HandOverLinkTooltip
                                .WithDialogOnComplete()
                //TODO: Enable in-person signing

                                // Customizing Layout
                                .WithCeremonyLayoutSettings(CeremonyLayoutSettingsBuilder.NewCeremonyLayoutSettings()
                                        .WithoutGlobalNavigation()
                                        .WithoutBreadCrumbs()
                                        .WithoutSessionBar()
                                        .WithTitle()))

                // Define the insured first and last name
                .WithSigner(SignerBuilder.NewSignerWithEmail(emailAddress)
                                         .WithCustomId(CUSTOM_ID)
                                         .WithFirstName(firstName)
                                         .WithLastName(lastName))
                //TODO: Add an additional signer

                // Define the document
                .WithDocument(DocumentBuilder.NewDocumentNamed("Insurance Form")
                                    .FromStream(file, DocumentType.PDF)
                                    .EnableExtraction()
                                    .WithSignature(SignatureBuilder.SignatureFor(emailAddress)
                                                                   .WithName("InsuredSignature")
                                                                   .EnableExtraction()

                                                                   // Bound fields
                                                                   
                                                                   //TODO: Add a textField with name "ExtraInfo"

                                                                   .WithField(FieldBuilder.SignatureDate()
                                                                                .WithPositionExtracted()
                                                                                .WithName("Date"))

                                                                   .WithField(FieldBuilder.CheckBox()
                                                                                .WithPositionExtracted()
                                                                                .WithName("checkbox")))

                                     //TODO: Add another signature using x-y coordinates for your new signer

                                     // Below are the form fields filled in by the Insured/customer
                                    .WithInjectedField(FieldBuilder.TextField()
                                                            .WithName("CustomerId")
                                                            .WithValue(insuredInitials))
                                    .WithInjectedField(FieldBuilder.TextField()
                                                            .WithName("FirstName")
                                                            .WithValue(firstName))
                                    .WithInjectedField(FieldBuilder.TextField()
                                                            .WithName("LastName")
                                                            .WithValue(lastName))
                                    .WithInjectedField(FieldBuilder.TextField()
                                                            .WithName("Address")
                                                            .WithValue(address))
                                    .WithInjectedField(FieldBuilder.TextField()
                                                            .WithName("City")
                                                            .WithValue(city))
                                    .WithInjectedField(FieldBuilder.TextField()
                                                            .WithName("Province")
                                                            .WithValue(province))
                                    .WithInjectedField(FieldBuilder.TextField()
                                                            .WithName("VehicleModel")
                                                            .WithValue(vehicleModel))
                                    .WithInjectedField(FieldBuilder.TextField()
                                                            .WithName("VehicleMake")
                                                            .WithValue(vehicleModel))
                                    .WithInjectedField(FieldBuilder.TextField()
                                                            .WithName("VehicleModelYear")
                                                            .WithValue(vehicleModelYear))

                                    //TODO: merge injected field "VehicleColor"

            ).Build();

            // Issue the request to the e-SignLive server to create the DocumentPackage
            PackageId packageId = client.CreatePackage(documentPackage);
            client.SendPackage(packageId);

            return packageId;
        }

        public static SessionToken createSessionToken(PackageId packageId, string API_KEY, string BASE_URL)
        {
            EslClient client = new EslClient(API_KEY, BASE_URL);

            SessionToken sessionToken = client.SessionService.CreateSessionToken(packageId, CUSTOM_ID);
            Trace.WriteLine("Session token: " + sessionToken.Token);

            return sessionToken;
        }
    }
}
