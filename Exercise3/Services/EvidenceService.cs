﻿using System;
using Silanis.ESL.SDK;
using System.Diagnostics;

namespace Exercise3.Services
{
    //Downloads the evidence summary pdf document
    public class EvidenceService
    {
        public byte[] downloadEvidenceSummary(String packageId, string API_KEY, string BASE_URL)
        {
            PackageId pkgId = new PackageId(packageId);

            EslClient client = new EslClient(API_KEY, BASE_URL);
            Trace.WriteLine("eSL client for download evidence summary created..");

            byte[] evidenceContent = client.DownloadEvidenceSummary(pkgId);
            Trace.WriteLine("Evidence content retrieved..");
            Trace.WriteLine("Length of byte array: " + evidenceContent.Length);

            return evidenceContent;
        }
    }

}